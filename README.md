Dataset Details (table, features, rows) and link:

This dataset is from the Big Query. The objective of the dataset is to explore the projects, project repositories, project dependencies and size, number of stars of repositories associated with each project.
The datasets consist of 7 Tables which describes below:
1.	Dependencies
2.	Projects   
3.	Projects_with_repositories 
4.	Repositories 
5.	Repository_depensencies 
6.	Tags 
7.	Versions 


Research Questions

1. What are the number of repositories, avg project size, and avg number of stars from 2007 to 2018?
2. What are the number of dependencies of projects each year whose tags were published from 2007 to 2018?
3. Which are the top 50 projects that are deprecated?
4. Prediction of what will be the number of dependencies of projects that are at top per platform?
5. Prediction of number of repositories from year, average number of stars and average project size (Machine Learning Algorithm)?

Project Configuration Details

Queries are run by connecting colab with bigquery. Specific cells of colab are run to give output of queries. These queries are visualized by matplotlib library of python. Bigquery client and panda library of python is used to create tables for the training of ML model. Model is also created using the client. Model and tables of query results for modelling of system (predictor) automatically get created in bigquery project section that is being used for running queries in bigquery.

For running the project successfully, follow following steps:
1. Connect colab to bigquery project
2. Connect to bigquery Client
3. Load necessary Python libraries cells for visualisation
4. Run all queries first and then visualisation cells of those queries for better understanding of datasets and results
5. Run Model creation cell for ML
6. Train Model using the tables made for ML in bigquery project section
7. Check status info for better understanding of the training
8. Evaluate results and predictions

Snapshot (visualization) Links:

Relation between Total Number of Repositories and Year:
https://gitlab.com/HabibaAtique/io-libraries-hub/-/blob/main/1.JPG

Relation between Average Size of Repositories and Year:
https://gitlab.com/HabibaAtique/io-libraries-hub/-/blob/main/2.JPG

Relation between Average number of stars of Repositories and Year:
https://gitlab.com/HabibaAtique/io-libraries-hub/-/blob/main/3.JPG

Relation between Total Number of Repositories, Average Size, Average number of stars of Repositories and Year:
https://gitlab.com/HabibaAtique/io-libraries-hub/-/blob/main/4.JPG

Plot b/w dependencies and project id:
https://gitlab.com/HabibaAtique/io-libraries-hub/-/blob/main/6.JPG

Top five projects and their dependencies:
https://gitlab.com/HabibaAtique/io-libraries-hub/-/blob/main/7.JPG
